import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeMock;
import org.junit.*;

import java.util.List;

import static AngajatiApp.controller.DidacticFunction.*;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

public class EmployeeRepoTest {
    private EmployeeMock emplRepo;

    @Before
    public void setUp() {
        emplRepo = new EmployeeMock();
    }

    @After
    public void tearDown() {
        emplRepo = null;
    }

    @Test
    public void TC1() {
        Employee e = new Employee();
        e.setId(145);
        e.setLastName("anda");
        e.setFirstName("popescu");
        e.setCnp("1245123654102");
        e.setFunction(TEACHER);
        e.setSalary(3500.0);
        //verificam daca intr-adevar angajatul s-a adaugat cu succes
        assertTrue(emplRepo.addEmployee(e));

    }
//cnp-ul nu e bun
    @Test
    public void TC2() {
        Employee e = new Employee();
        e.setId(123);
        e.setLastName("anda");
        e.setFirstName("popescu");
        e.setCnp("hgfytfyfufu");
        e.setFunction(ASISTENT);
        e.setSalary(2800.0);
        //verificam daca intr-adevar angajatul s-a adaugat cu succes
        assertFalse(emplRepo.addEmployee(e));
    }
/*id-ul nu e bun
    @Test
    public void TE3() {
        Employee e = new Employee();
        e.setId(45);
        e.setLastName("anda");
        e.setFirstName("popescu");
        e.setCnp("1245890012347");
        e.setFunction(CONFERENTIAR);
        e.setSalary(7800.0);
        //verificam daca intr-adevar angajatul s-a adaugat cu succes
        assertFalse(emplRepo.addEmployee(e));
    }
//numele nu e bun
    @Test
    public void TE4() {
        Employee e = new Employee();
        e.setId(234);
        e.setLastName(null);
        e.setFirstName("popescu");
        e.setCnp("1245780032001");
        e.setFunction(LECTURER);
        e.setSalary(3500.0);
        //verificam daca intr-adevar angajatul s-a adaugat cu succes
        assertFalse(emplRepo.addEmployee(e));
    }*/
//salariul nu e bun
    @Test
    public void TC5() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("anda");
        e.setFirstName("pop");
        e.setCnp("4587100236910");
        e.setFunction(ASISTENT);
        e.setSalary(13505.8);
        //verificam daca intr-adevar angajatul s-a adaugat cu succes
        assertFalse(emplRepo.addEmployee(e));
    }
    //testare metoda modifyEmployeeFunction
    @Test
    public void TC6(){
        Employee Marius = new Employee(123,"Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        Employee Ion = new Employee(145, "Ion", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
        Employee Gicu = new Employee(234,"Gicu", "Ionescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
        Employee Dodel = new Employee(564, "Dodel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        Employee Dorel = new Employee(325, "Dorel", "Georgescu", "1234567890876", DidacticFunction.TEACHER, 2500d);
        Employee Larson   = new Employee(254, "Larson", "Puscas", "1234567890876", DidacticFunction.TEACHER,  2500d);
        emplRepo.modifyEmployeeFunction(Ion, TEACHER);
        assertEquals(TEACHER, emplRepo.findEmployeeById(1).getFunction());
    }

    @Test
    public void TC7(){
        Employee Andrei = null;
        List<Employee> listE = emplRepo.getEmployeeList();
        emplRepo.modifyEmployeeFunction(Andrei, LECTURER);
        assertTrue(listE.equals(emplRepo.getEmployeeList()));
    }
}